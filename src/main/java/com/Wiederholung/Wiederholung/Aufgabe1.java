/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Wiederholung.Wiederholung;

import java.util.Scanner;

/**
 *
 * @author georg
 */
public class Aufgabe1 {
    
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int durchmesser = s.nextInt();
        double radius = durchmesser/2;
        System.out.println("Radius: " + Math.PI * durchmesser);
        System.out.println("Fläche: " + Math.PI * radius * radius);
    }
    
}
