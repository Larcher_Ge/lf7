/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Wiederholung.Wiederholung;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author georg
 */
public class Aufgabe5 {
    public static void main(String[] args) {
        int[] arr = new int[1000];
        ArrayList<Integer> pos = new ArrayList<>();
        int counterZahl = 0;
        Random r = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = r.nextInt(101);
        }
        Scanner s = new Scanner(System.in);
        int zahl = s.nextInt();
        for (int i = 0; i < arr.length; i++) {
               if(arr[i] == zahl) {
                   counterZahl++;
                   pos.add(i);
               }
            
        }
        
        System.out.println("Die Zahl : " + zahl + " ist: " + counterZahl + " vorgekommen an den Positionen: ");
        for (int i : pos) {
            System.out.print(i +", ");
        }
    }
    
}
