/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Wiederholung.Wiederholung;

import java.util.Random;

/**
 *
 * @author georg
 */
public class Aufgabe4 {

    public static void main(String[] args) {

        int kopf = 0;
        int zahl = 0;
        int maxlaengeKopf = 0;
        int maxlaengeZahl = 0;
        int currentlaengeKopf = 0;
        int currentlaengeZahl = 0;

        for (int i = 0; i < 100; i++) {
            Random r = new Random();
            int rdm = r.nextInt(2);
            if (rdm == 0) {
                currentlaengeKopf++;
                currentlaengeZahl = 0;
                kopf++;
            } else {
                currentlaengeZahl++;
                currentlaengeKopf = 0;
                zahl++;
            }

            if (currentlaengeKopf > maxlaengeKopf) {
                maxlaengeKopf = currentlaengeKopf;
            }
            if (currentlaengeZahl > maxlaengeZahl) {
                maxlaengeZahl = currentlaengeZahl;
            }

        }

        System.out.println("Die längste Reihenfolge mit Kopf mit:" + maxlaengeKopf);
        System.out.println("Die längste Reihenfolge mit  Zahl mit:" + maxlaengeZahl);

        System.out.println(kopf + " " + zahl);

    }

}
